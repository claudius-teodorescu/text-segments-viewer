import { css } from "https://cdn.jsdelivr.net/npm/lit/+esm";

export const styles =
    css`
        :host {
            width: var(--kub-width, 40vw);
        }

        div.segment {
            padding: 5px;
            width: 100%;
        }

        div.segment:nth-child(even) {
            background: #cccccc;
        }

        div.segment:nth-child(odd) {
            background: #fffff;
        }
`;