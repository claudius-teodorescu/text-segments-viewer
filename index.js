
import { LitElement, html } from "https://cdn.jsdelivr.net/npm/lit/+esm";
import { map } from "https://cdn.jsdelivr.net/npm/lit/directives/map.js/+esm";
import "https://cdn.jsdelivr.net/npm/@lion/pagination@0.9.1/lion-pagination.js/+esm";
import { styles } from "./styles.js";

export default class TextSegmentsViewer extends LitElement {
    static properties = {
        _visibleTextSegments: {
            attribute: false,
        },
        heading: {
            attribute: false,
        },
        itemsPerPage: {
            attribute: false
        },
        currentPage: {
            attribute: false
        },
        pageCount: {
            attribute: false
        },
    };

    static styles = styles;

    constructor() {
        super();

        this._init();
    }

    render() {
        return html`
            <sl-details disabled>
                <div slot="summary">
                    <output>
                        ${this._displaySummary()}
                    </output>                
                </div>
                <div>
                    <lion-pagination count="${this.pageCount}" current="${this.currentPage}"></lion-pagination>
                    <div id="content">
                        ${map(this._visibleTextSegments, (item, index) => this._displayTextSegment(index, item))}
                    </div>
                </div>
            </sl-details>   
        `;
    }

    createRenderRoot() {
        let root = super.createRenderRoot();

        this.addEventListener("sl-show", () => {
            this.dispatchEvent(new CustomEvent("kub-text-segments-viewer:show", {
                "bubbles": true,
                "composed": true,
            }));
        });

        this.addEventListener("sl-hide", () => {
            if (this._reset) {
                this._reset = false;
            } else {
                root.dispatchEvent(new CustomEvent("kub-text-segments-viewer:hide", {
                    "bubbles": true,
                    "composed": true,
                }));
            }
        });

        return root;
    }

    firstUpdated() {
        this.details = this.renderRoot.querySelector("sl-details");
        this.summary = this.renderRoot.querySelector("div[slot = 'summary']");
        this.pagination = this.renderRoot.querySelector("lion-pagination");
        this.content = this.renderRoot.querySelector("div#content");

        this.pagination.addEventListener("current-changed", event => {
            let currentPage = event.target.current;
            this._displayPage(currentPage);
            this.currentPage = currentPage;
        });
    }

    _init() {
        this._textSegmentLocators = [];
        this._visibleTextSegments = [];
        this.heading = "";
        this.itemsPerPage = 10;
        this.currentPage = 1;
        this.pageCount = 1;
        this._reset = false;
    }

    set textSegmentLocators(value) {
        this._textSegmentLocators = value;
        this.pageCount = Math.ceil(value.length / this.itemsPerPage);

        this._displayPage(1);
    }

    _displaySummary() {
        if (this._textSegmentLocators.length === 0) {
            return html``;
        } else {
            return html`
                <span>${this.heading},</span>
                <span>${this._textSegmentLocators.length.toLocaleString("ro")}</span>
                <span>segment(e)</span>
            `;
        }
    }

    _displayPage(currentPage) {
        let startIndex = (currentPage - 1) * this.itemsPerPage;
        let endIndex = currentPage * this.itemsPerPage;
        this._visibleTextSegments = this._textSegmentLocators.slice(startIndex, endIndex);
    }

    _displayTextSegment(index, locator) {
        let globalIndex = this.itemsPerPage * (this.currentPage - 1) + index + 1;

        return html`
            <div class="segment">
                <div class="segment-toolbar">${globalIndex}. <a href="${locator}" target="_blank">${locator}</a></div>
                <sl-include src="${locator}"></sl-include>
            </div>
        `;
    }

    show() {
        this.details.disabled = false;
        this.details.show();
    }

    hide() {
        this.details.hide();
    }

    reset() {
        this._init();        
        this._reset = true;
        this.details.hide();
        this.details.disabled = true;
    }
};

window.customElements.define("kub-text-segments-viewer", TextSegmentsViewer);
